import pandas as pd
import mysql.connector
from normalizes import result_dfD


conn = mysql.connector.connect(
    host="localhost",
    user="John",
    password="docu",
    database="restau_rant"
)

# Fonction pour obtenir l'ID d'un ingrédient ou d'une recette (ajoute si non existant)
def get_id(cursor, table, column, value):
    cursor.execute(f"SELECT id_{table} FROM {table} WHERE {column} = %s", (value,))
    result = cursor.fetchone()

    if result:
        return result[0]
    else:
        cursor.execute(f"INSERT INTO {table} ({column}) VALUES (%s)", (value,))
        conn.commit()
        return cursor.lastrowid


mycursor = conn.cursor(buffered=True)

for index, row in result_dfD.iterrows():
    ingredient = row['ingredient'].strip()
    spoon = int(row['spoon'])
    recips_name = row['name'].strip()

    id_recips = get_id(mycursor, 'recips', 'recips_name', recips_name)
    id_products = get_id(mycursor, 'Products', 'name', ingredient)

    # Vérifier si l'entrée existe déjà dans la table de liaison
    mycursor.execute("SELECT * FROM recips_prod WHERE id_recips = %s AND id_products = %s", (id_recips, id_products))
    existing_entry = mycursor.fetchall()

    if not existing_entry:
        mycursor.execute("INSERT INTO recips_prod (id_recips, id_products, spoon) VALUES (%s, %s, %s)", (id_recips, id_products, spoon))
        conn.commit()


mycursor.close()
conn.close()        

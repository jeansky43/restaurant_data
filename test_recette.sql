USE restau_rant;

-- Insertion de 'salade de fruit' dans la table recips
INSERT INTO recips (recips_name) VALUES ('salade de fruit');

-- Récupération de l'ID de 'salade de fruit'
SELECT id_recips INTO @id_recips FROM recips WHERE recips_name = 'salade de fruit';

-- Insertion des produits s'ils n'existent pas déjà
INSERT INTO Products (name) VALUES ('sucre') ON DUPLICATE KEY UPDATE name = VALUES(name);
INSERT INTO Products (name) VALUES ('amine') ON DUPLICATE KEY UPDATE name = VALUES(name);

-- Insertion dans recips_prod
INSERT INTO recips_prod (id_products, id_recips, spoon)
SELECT id_products, @id_recips, spoon FROM (
    SELECT id_products, name FROM Products WHERE name IN ('vanille', 'miel', 'sucre', 'canelle poudre', 'amine')
) AS prod_names
JOIN (
    SELECT 'vanille' AS name, 1 AS spoon
    UNION ALL
    SELECT 'miel', 3
    UNION ALL
    SELECT 'sucre', 2
    UNION ALL
    SELECT 'canelle poudre', 4
    UNION ALL
    SELECT 'amine', 29
) AS spoons ON prod_names.name = spoons.name;

USE restau_rant;

-- Insertion de 'crepe' dans la table recips
INSERT INTO recips (recips_name) VALUES ('crepe');

-- Récupération de l'ID de 'crepe'
SELECT id_recips INTO @id_recips FROM recips WHERE recips_name = 'crepe';

-- Insertion des produits s'ils n'existent pas déjà
INSERT INTO Products (name) VALUES ('farine') ON DUPLICATE KEY UPDATE name = VALUES(name);
INSERT INTO Products (name) VALUES ('sucre roux') ON DUPLICATE KEY UPDATE name = VALUES(name);
INSERT INTO Products (name) VALUES ('oeuf') ON DUPLICATE KEY UPDATE name = VALUES(name);
INSERT INTO Products (name) VALUES ('lait') ON DUPLICATE KEY UPDATE name = VALUES(name);

-- Insertion dans recips_prod pour la recette 'crepe'
INSERT INTO recips_prod (id_products, id_recips, spoon)
SELECT id_products, @id_recips, spoon FROM (
    SELECT id_products, name FROM Products WHERE name IN ('farine', 'sucre roux', 'oeuf', 'lait')
) AS prod_names
JOIN (
    SELECT 'farine' AS name, 20 AS spoon
    UNION ALL
    SELECT 'sucre roux', 2
    UNION ALL
    SELECT 'oeuf', 2
    UNION ALL
    SELECT 'lait', 5
) AS spoons ON prod_names.name = spoons.name;


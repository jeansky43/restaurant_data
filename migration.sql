USE restau_rant;

CREATE TABLE `recips` (
    `id_recips` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `recips_name` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id_recips`)
);


CREATE TABLE `recips_prod` (
    `id_products` INT UNSIGNED NOT NULL,
    `id_recips` INT UNSIGNED NOT NULL,
    `spoon` INT NOT NULL,
    FOREIGN KEY (`id_products`) REFERENCES `Products` (`id_products`),
    FOREIGN KEY (`id_recips`) REFERENCES `recips` (`id_recips`)
); 
import pandas as pd
import mysql.connector
from normalizes import result_dfA


conn = mysql.connector.connect(
    host="localhost",
    user="John",
    password="docu",
    database="restau_rant"
)


mycursor = conn.cursor(buffered=True)

# Ajout des restaurants à la table Storage
restaurants = ['Restaurant A', 'Restaurant B', 'Restaurant C']  
for restaurant in restaurants:
    query = "INSERT INTO Storage (name_store) VALUES (%s)"
    mycursor.execute(query, (restaurant,))

# Insérer les données dans la table 'Products'
for _, row in result_dfA.iterrows():
    query = "INSERT INTO Products (name) VALUES (%s)"
    values = (row['product'],)
    mycursor.execute(query, values)

conn.commit()

# Récupération des id_products correspondant aux produits du DataFrame
id_products = []
for _, row in result_dfA.iterrows():
    query = "SELECT id_products FROM Products WHERE name = %s"
    mycursor.execute(query, (row['product'],))
    result = mycursor.fetchone()  
    if result:
        id_products.append(result[0])


# Récupération de l'id_store du restaurant A depuis la table Storage
restaurant_name = 'Restaurant A'  
query = "SELECT id_store FROM Storage WHERE name_store = %s"
mycursor.execute(query, (restaurant_name,))
result = mycursor.fetchone()

if result:
    restaurant_id = result[0]  
    # Insertion des données dans la table Stock_Qt
    for i, stock_value in enumerate(result_dfA['stock']):
        query = """
        INSERT INTO Stock_Qt (id_products, id_store, qt) VALUES (%s, %s, %s)
        ON DUPLICATE KEY UPDATE qt = %s
        """
        values = (id_products[i], restaurant_id, stock_value, stock_value)
        mycursor.execute(query, values)
    conn.commit()
else:
    print("Restaurant A n'existe pas dans la table Storage.")


mycursor.close()
conn.close()
    




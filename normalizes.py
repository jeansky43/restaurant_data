import pandas as pd
from unidecode import unidecode

# Normalisation data_a

def process_data_a(file_path):
    # Charger le fichier CSV
    df = pd.read_csv(file_path)

    # Convert 'stock' column to numeric type
    df['stock'] = pd.to_numeric(df['stock'], errors='coerce')

    # Fonction jaime_pas_les_mots
    def jaime_pas_les_mots(l):
        nouvelle_list = []
        for expr in l:
            les_mots = expr.split()
            nouvelle_expr = []
            for mot in les_mots:
                # tout en minuscule
                tmp = mot.lower()
                # enlever les 's' à la fin des mots
                tmp = tmp[:-1] if tmp.endswith('s') else tmp
                # enlever les accents et les caractères spéciaux
                tmp = unidecode(tmp)
                nouvelle_expr.append(tmp)
            nouvelle_list.append(" ".join(nouvelle_expr))
        return nouvelle_list

    # Appliquer la fonction à la colonne 'product' du DataFrame
    df['product'] = jaime_pas_les_mots(df['product'])

    # Assurer que la colonne 'stock' est de type int
    df['stock'] = df['stock'].astype(int)

    # Faire la somme des quantités pour les produits ayant le même nom
    sum_by_product = df.groupby('product')['stock'].sum().reset_index()    

    return sum_by_product

result_dfA = process_data_a('data/extract_A.csv')



# Normalisation data_b

import pandas as pd
from unidecode import unidecode

def process_data_b(csv_path):
    df = pd.read_csv(csv_path)

    # Convert 'inventory' column to numeric type
    df['inventory'] = pd.to_numeric(df['inventory'], errors='coerce')

    # Fonction jaime_pas_les_mots
    def jaime_pas_les_mots(l):
        nouvelle_list = []
        for expr in l:
            les_mots = expr.split()
            nouvelle_expr = []
            for mot in les_mots:
                # tout en minuscule
                tmp = mot.lower()
                # enlever les s à la fin des mots
                tmp = tmp[:-1] if tmp.endswith('s') else tmp
                # enlever les accents et les trucs louches (genre les accents et les œ)
                tmp = unidecode(tmp)
                nouvelle_expr.append(tmp)
            nouvelle_list.append(" ".join(nouvelle_expr))
        return nouvelle_list

    # Appliquer la fonction à la colonne spécifique du DataFrame
    df['stock'] = jaime_pas_les_mots(df['stock'])

    # Convertir la colonne 'date' en datetime
    df['date'] = pd.to_datetime(df['date'], dayfirst=True, errors='coerce')

    # Assurer que la colonne 'stock' est de type int
    df['inventory'] = df['inventory'].astype(int)

    # Faire la somme des quantités pour les produits ayant le même nom
    sum_by_stock = df.sort_values(by='date').groupby('stock').agg({'date': 'last', 'inventory': 'last'}).reset_index()

    return sum_by_stock

result_dfB = process_data_b('data/extract_B.csv')


# Normalisation data_c

import pandas as pd
from unidecode import unidecode

def process_data_c(csv_path):
    df = pd.read_csv(csv_path, names=['product', 'quantity'])

    # Fonction jaime_pas_les_mots
    def jaime_pas_les_mots(l):
        nouvelle_list = []
        for expr in l:
            les_mots = expr.split()
            nouvelle_expr = []
            for mot in les_mots:
                # tout en minuscule
                tmp = mot.lower()
                # enlever les s à la fin des mots
                tmp = tmp[:-1] if tmp.endswith('s') else tmp
                # enlever les accents et les trucs louches (genre les accents et les œ)
                tmp = unidecode(tmp)
                nouvelle_expr.append(tmp)
            nouvelle_list.append(" ".join(nouvelle_expr))
        return nouvelle_list

    # Appliquer la fonction à la colonne spécifique du DataFrame
    df['product'] = jaime_pas_les_mots(df['product'])

    # Convert negative values to 0
    df['quantity'] = df['quantity'].apply(lambda x: max(0, x))

    # Faire la somme des quantités pour les produits ayant le même nom
    # Assurer que la colonne 'quantity' est de type int
    df['quantity'] = df['quantity'].astype(int)

    # Faire la somme des quantités pour les produits ayant le même nom
    sum_by_prod = df.groupby('product')['quantity'].sum().reset_index()

    return sum_by_prod

result_dfC = process_data_c('data/extract_C.csv')



# NORMALISATION RECIPES

import pandas as pd
from unidecode import unidecode

def process_data_recipes(csv_path):
    df4 = pd.read_csv("data/recettes.csv")

# Convert 'spoon' column to numeric type
    df4['spoon'] = pd.to_numeric(df4['spoon'], errors='coerce')

# Fonction jaime_pas_les_mots
    def jaime_pas_les_mots(l):
        nouvelle_list = []
        for expr in l:
            les_mots = expr.split()
            nouvelle_expr = []
            for mot in les_mots:
            # tout en minuscule
                tmp = mot.lower()
            # enlever les s à la fin des mots
                tmp = tmp[:-1] if tmp.endswith('s') else tmp
            # enlever les accents et les trucs louches (genre les accents et les œ)
                tmp = unidecode(tmp)
                nouvelle_expr.append(tmp)
            nouvelle_list.append(" ".join(nouvelle_expr))
        return nouvelle_list

# Appliquer la fonction jaime_pas_les_mots à la colonne 'name' de df4
    df4['name'] = jaime_pas_les_mots(df4['name'])
    df4['ingredient'] = jaime_pas_les_mots(df4['ingredient'])

# Remove double quotes from the 'name' column
    df4['name'] = df4['name'].str.replace('"', '')

    dfD = df4
    return dfD

result_dfD = process_data_recipes('data/recettes.csv')















DROP DATABASE IF EXISTS restau_rant;
CREATE DATABASE restau_rant;
USE restau_rant;

GRANT ALL PRIVILEGES ON restau_rant.* TO 'John'@'localhost';

CREATE TABLE `Products` (
    `id_products` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL
);

CREATE TABLE `Storage` (
    `id_store` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name_store` VARCHAR(255) NOT NULL
);

CREATE TABLE `Stock_Qt` (
    `id_products` INT UNSIGNED NOT NULL,
    `id_store` INT UNSIGNED NOT NULL,
    `qt` INT NOT NULL,
    FOREIGN KEY (`id_products`) REFERENCES `Products`(`id_products`),
    FOREIGN KEY (`id_store`) REFERENCES `Storage`(`id_store`),
    PRIMARY KEY (`id_products`, `id_store`)
);



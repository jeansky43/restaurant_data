from typing import List
from pydantic import BaseModel


class Products(BaseModel):
    id_products: int | None = None
    name: str 

class recips(BaseModel):
    id_recips: int | None = None
    recips_name: str 
    ingredient: list

class Storage(BaseModel):
     id_store: int | None = None
     name_store: str
     qt: int

class global_stock(BaseModel):
    stock_resto: list[Storage]
    tatol_stock: int

class Stock_Qt(BaseModel):
    id_products: int | None = None
    id_store: int | None = None
    qt: int | None = None

class NewProduct(BaseModel):
    name: str

class StockUpdate(BaseModel):
    quantity: int



from typing import Union
from fastapi import FastAPI, HTTPException
import mysql.connector
from model_dto import Products, recips, Storage, global_stock, NewProduct, StockUpdate, Stock_Qt

conn = mysql.connector.connect(
    host="localhost",
    user="John",
    password="docu",
    database="restau_rant"
)

mycursor = conn.cursor(dictionary=True)
app = FastAPI()

# Test API
@app.get("/Products")
def get_products():
    query = "SELECT * FROM Products"
    mycursor.execute(query,)
    return mycursor.fetchall()

# STOCK_QUANTITY
@app.get("/Stock_Qt/{name}")
def get_product_quantity(name: str):  
   
    
    query = """
        SELECT p.name , s.name_store , SUM(sq.qt) AS quantity
        FROM Products p
        JOIN Stock_Qt sq ON p.id_products = sq.id_products
        JOIN Storage s ON sq.id_store = s.id_store
        WHERE p.name = %s
        GROUP BY p.name, s.name_store
    """
    mycursor.execute(query, (name,))
    results = mycursor.fetchall()

    
    mycursor.close()
    conn.close()

    return results

# UPDATE
@app.post("/Products/")
async def create_product(new_product: NewProduct):   

    try:
        
        query = "INSERT INTO Products (name) VALUES (%s)"
        mycursor.execute(query, (new_product.name,))
        conn.commit()

        new_product_id = mycursor.lastrowid
        
        return {"id": new_product_id, "name": new_product.name}

    finally:
        
        mycursor.close()
        conn.close()

# AUGMENTER OU DIMINUER
@app.put("/Stock_Qt/{id_products}/{id_store}")
async def update_stock(id_products: int, id_store: int, stock_update: StockUpdate):
    
    if stock_update.quantity < 0:
        raise HTTPException(status_code=400, detail="La quantité ne peut pas être négative")
    
    try:
        
        mycursor.execute("SELECT id_products FROM Products WHERE id_products = %s", (id_products,))
        if not mycursor.fetchone():
            raise HTTPException(status_code=404, detail="Produit non trouvé")

        mycursor.execute("SELECT id_store FROM Storage WHERE id_store = %s", (id_store,))
        if not mycursor.fetchone():
            raise HTTPException(status_code=404, detail="Restaurant non trouvé")

        
        mycursor.execute(
            "SELECT qt FROM Stock_Qt WHERE id_products = %s AND id_store = %s",
            (id_products, id_store)
        )
        current_quantity = mycursor.fetchone()

        if not current_quantity:
            raise HTTPException(status_code=404, detail="Le produit n'est pas en stock dans ce restaurant")

        
        new_quantity = current_quantity[0] + stock_update.quantity

        if new_quantity < 0:
            raise HTTPException(status_code=400, detail="La quantité ne peut pas devenir négative")

        
        mycursor.execute(
            "UPDATE Stock_Qt SET qt = %s WHERE id_products = %s AND id_store = %s",
            (new_quantity, id_products, id_store)
        )
        conn.commit()

        return {"message": "Stock mis à jour avec succès"}

    finally:
        
        mycursor.close()
        conn.close()

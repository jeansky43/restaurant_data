import pandas as pd
import mysql.connector
from normalizes import result_dfC


conn = mysql.connector.connect(
    host="localhost",
    user="John",
    password="docu",
    database="restau_rant"
)

# 
mycursor = conn.cursor(buffered=True)

# Récupération de l'ID du restaurant C depuis la table Storage
restaurant_name = 'Restaurant C'  
query = "SELECT id_store FROM Storage WHERE name_store = %s"
mycursor.execute(query, (restaurant_name,))
result = mycursor.fetchone()

if result:
    restaurant_id = result[0]  
    for _, row in result_dfC.iterrows():
        # Vérification de l'existence du produit dans la table Products
        query = "SELECT id_products FROM Products WHERE name = %s"
        mycursor.execute(query, (row['product'],))
        existing_product = mycursor.fetchone()
        
        if not existing_product:
            # Si le produit n'existe pas, l'ajouter à la table Products
            insert_product_query = "INSERT INTO Products (name) VALUES (%s)"
            mycursor.execute(insert_product_query, (row['product'],))
            conn.commit()

            # Récupération de l'ID du nouveau produit ajouté
            mycursor.execute(query, (row['product'],))
            new_product_id = mycursor.fetchone()[0]
        else:
            new_product_id = existing_product[0]

        # Vérification de l'existence de la combinaison id_products et id_store dans Stock_Qt
        check_existing_stock_query = "SELECT qt FROM Stock_Qt WHERE id_products = %s AND id_store = %s"
        mycursor.execute(check_existing_stock_query, (new_product_id, restaurant_id))
        existing_stock = mycursor.fetchone()

        if existing_stock:
            # Si la combinaison existe déjà, mettre à jour la quantité
            update_stock_query = "UPDATE Stock_Qt SET qt = %s WHERE id_products = %s AND id_store = %s"
            mycursor.execute(update_stock_query, (row['quantity'], new_product_id, restaurant_id))
            conn.commit()
        else:
            # Si la combinaison n'existe pas, insérer une nouvelle entrée
            insert_stock_query = "INSERT INTO Stock_Qt (id_products, id_store, qt) VALUES (%s, %s, %s)"
            mycursor.execute(insert_stock_query, (new_product_id, restaurant_id, row['quantity']))
            conn.commit()


mycursor.close()
conn.close()
